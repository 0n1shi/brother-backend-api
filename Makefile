.PHONY: build
build:
	go build

.PHONY: run
run:
	bee run

.PHONY: doc
doc:
	bee run -downdoc=true -gendoc=true